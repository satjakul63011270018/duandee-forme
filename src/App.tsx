import "./App.css";
import {
  createBrowserRouter,
  RouterProvider,
  // Outlet,
} from "react-router-dom";
import Home from "./pages/landing/home/Home";
import LineAuth from "./pages/liff/auth/LineAuth";
import Register from "./pages/liff/register/Register";
import Horoscope from "./pages/liff/horoscope/Horoscope";

// import Header from './components/landing/header/Header';
// import Menu from './components/landing/menu/Menu';
// import Footer from './components/landing/footer/Footer';

function App() {
  const router = createBrowserRouter([
    {
      path: "/",
      element: <Home />,
      // loader: rootLoader,
    },
    {
      path: "/auth/liff",
      element: <LineAuth />,
    },
    {
      path: "/auth/liff/register",
      element: <Register />,
    },
    {
      path: "/auth/liff/horoscope",
      element: <Horoscope />,
    },
  ]);

  return <RouterProvider router={router} />;
}

export default App;
