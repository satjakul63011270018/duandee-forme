import React from "react";
import "./CardPaper.css";
interface Props {
  img?: string;
  title?: string;
  url?: string;
}
const CardPaper: React.FC<Props> = ({ img, title, url }) => {
  return (
    <div className="CardPaper">
      <div className="card border-0">
        <div className="card-body p-0 m-0">
          <div className="d-flex justify-content-center">
            <img src={`/public/assets/img/dashboard/${img}`} alt="" />
          </div>
          <div className="d-flex justify-content-center">
            <a type="button" href={url} className="btn">
              {title}
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CardPaper;
