import React, { useState } from "react";
import type { TabsProps } from "antd";
import "./tab.css";
import CardTabBody from "./CardTabBody";
const Tab = () => {
  const [tab, setTab] = useState("1");
  const handleTabClick = (tabNumber: string) => {
    setTab(tabNumber);
  };
  return (
    <div>
      <div className="tab">
        <div className="d-flex justify-content-center align-items-center h-100 pt-5">
          <div className="card ">
            <div className="card-body p-0">
              <div className="d-flex">
                <button
                  type="button"
                  onClick={() => handleTabClick("1")}
                  className={`btn border-0 btn-1 ${
                    tab === "1" ? "btn-active" : ""
                  }`}
                >
                  การงาน
                </button>
                <button
                  type="button"
                  onClick={() => handleTabClick("2")}
                  className={`btn border-0 btn-2 ${
                    tab === "2" ? "btn-active" : ""
                  }`}
                >
                  การเงิน
                </button>
                <button
                  type="button"
                  onClick={() => handleTabClick("3")}
                  className={`btn border-0 btn-3 ${
                    tab === "3" ? "btn-active" : ""
                  }`}
                >
                  ความรัก
                </button>
                <button
                  type="button"
                  onClick={() => handleTabClick("4")}
                  className={`btn border-0 btn-4 ${
                    tab === "4" ? "btn-active" : ""
                  }`}
                >
                  สุขภาพ
                </button>
              </div>
              {tab === "1" && (
                <CardTabBody
                  img="card-zodiac-card-01.png"
                  title="ด้านการงาน"
                  username="คุณ User Name"
                  detail="ในช่วงเดือนนี้คุณอาจจะต้องทำงานที่เกี่ยวกับการเดินทาง
                มากขึ้นจะไปได้ค่อนข้างดี 
                หรืออาจได้เดินทางในเรื่องของ
                การงานได้ด้วยเช่นกัน 
                แต่ต้องระวังว่าคุณอาจทำงานใน
                ส่วนอื่นไม่ค่อยทันด้วยเช่นกัน 
                แต่ก็มีเกณฑ์ที่จะมีคนอายุน้อยกว่า 
                หรือเด็กฝึกงานเข้ามาช่วยรับภาระไป
                จากคุณได้ค่อนข้างดี"
                />
              )}
              {tab === "2" && (
                <CardTabBody
                  img="card-zodiac-card-01.png"
                  title="ด้านการเงิน"
                  username="คุณ User Name"
                  detail="ในช่วงเดือนนี้คุณอาจจะต้องทำงานที่เกี่ยวกับการเดินทาง
                มากขึ้นจะไปได้ค่อนข้างดี 
                หรืออาจได้เดินทางในเรื่องของ
                การงานได้ด้วยเช่นกัน 
                แต่ต้องระวังว่าคุณอาจทำงานใน
                ส่วนอื่นไม่ค่อยทันด้วยเช่นกัน 
                แต่ก็มีเกณฑ์ที่จะมีคนอายุน้อยกว่า 
                หรือเด็กฝึกงานเข้ามาช่วยรับภาระไป
                จากคุณได้ค่อนข้างดี"
                />
              )}
              {tab === "3" && (
                <CardTabBody
                  img="card-zodiac-card-01.png"
                  title="ด้านความรัก"
                  username="คุณ User Name"
                  detail="ในช่วงเดือนนี้คุณอาจจะต้องทำงานที่เกี่ยวกับการเดินทาง
                มากขึ้นจะไปได้ค่อนข้างดี 
                หรืออาจได้เดินทางในเรื่องของ
                การงานได้ด้วยเช่นกัน 
                แต่ต้องระวังว่าคุณอาจทำงานใน
                ส่วนอื่นไม่ค่อยทันด้วยเช่นกัน 
                แต่ก็มีเกณฑ์ที่จะมีคนอายุน้อยกว่า 
                หรือเด็กฝึกงานเข้ามาช่วยรับภาระไป
                จากคุณได้ค่อนข้างดี"
                />
              )}
              {tab === "4" && (
                <CardTabBody
                  img="card-zodiac-card-01.png"
                  title="ด้านสุขภาพ"
                  username="คุณ User Name"
                  detail="ในช่วงเดือนนี้คุณอาจจะต้องทำงานที่เกี่ยวกับการเดินทาง
                มากขึ้นจะไปได้ค่อนข้างดี 
                หรืออาจได้เดินทางในเรื่องของ
                การงานได้ด้วยเช่นกัน 
                แต่ต้องระวังว่าคุณอาจทำงานใน
                ส่วนอื่นไม่ค่อยทันด้วยเช่นกัน 
                แต่ก็มีเกณฑ์ที่จะมีคนอายุน้อยกว่า 
                หรือเด็กฝึกงานเข้ามาช่วยรับภาระไป
                จากคุณได้ค่อนข้างดี"
                />
              )}
              <div className="d-flex justify-content-center">
                <button type="button" className="btn btn-further-horoscope">
                  ดูดวงต่อ
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Tab;
