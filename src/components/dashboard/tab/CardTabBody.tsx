import React from "react";
import "./CardTabBody.css";
interface Props {
  img?: string;
  title?: string;
  username?: string;
  detail?: string;
}
const CardTabBody: React.FC<Props> = ({ img, title, username, detail }) => {
  return (
    <div className="card-tab-body">
      <div className="row h-100 w-100">
        <div className="w-50 h-100">
          <div className="d-flex justify-content-center align-items-center h-100 w-100">
            <img
              src={`/public/assets/img/dashboard/cardzodiac/${img}`}
              alt=""
              className="img-fluid ps-4"
            />
          </div>
        </div>
        <div className="w-50 pe-0">
          <div className="d-flex flex-column justify-content-center align-items-center h-100 w-100">
            <p className="text-username mb-0 pb-0">{username}</p>
            <p className="text-side mb-0 pb-0">{title}</p>
            <p className="text-detail mb-0 pb-0">{detail}</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CardTabBody;
