import React from "react";
import "./CardLuckyNumber.css";
const CardLuckyNumber = () => {
  return (
    <div className="card-blur">
      <div className="CardLuckyNumber mb-4 ms-2 me-2">
        <div className="card border-0">
          <div className="card-body p-0 m-0">
            <p className="text-center text-white pt-3 pb-0 mb-0">
              เลขนำโชคประจำเดือนนี้
            </p>
            <div className="d-flex justify-content-between ps-4 pe-1">
              <img
                src="/public/assets/img/dashboard/icon-lock.png"
                className="icon-lock"
                alt=""
              />
              <div className="d-flex flex-column justify-content-center w-100 mt-2 ps-3 pe-2">
                <img
                  src="/public/assets/img/dashboard/img-lucky-number.png"
                  alt=""
                />
                <div className="d-flex justify-content-center mt-2 ">
                  <button type="button" className="btn">
                    แลกเลย
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CardLuckyNumber;
