import React from "react";
import { Card, Space } from "antd";
import "./CardDailyAdvice.css";
const CardDailyAdvice = () => {
  return (
    <div className="cardDailyAdvice ms-2 me-2 mb-4">
      <Card title="คำแนะนำประจำวัน" className="text-center" bordered={false}>
        <Space>
          <p>
            “ ทิ้งความเชื่อเก่าๆไป เปิดรับความคิดใหม่ๆ อย่าลืมฟังเสียง
            คนรอบตัวของคุณ”
          </p>
          <img src="/public/assets/img/dashboard/icon-duandee.png" alt="" />
        </Space>
      </Card>
    </div>
  );
};

export default CardDailyAdvice;
