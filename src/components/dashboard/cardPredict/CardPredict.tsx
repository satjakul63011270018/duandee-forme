import React from "react";
import "./CardPredict.css";
import { RightOutlined } from "@ant-design/icons";

interface Props {
  img?: string;
  title?: string;
  url?: string;
}

const CardPredict: React.FC<Props> = ({ img, title, url }) => {
  return (
    <div className="CardPredict">
      <div className="card">
        <div className="card-header">
          <p className="text-center mb-0 pb-0">{title || "Card title"}</p>
        </div>
        <div className="card-body">
          <div className="d-flex justify-content-center mt-1 mb-1">
            <img src={`/public/assets/img/dashboard/${img}`} alt="" />
          </div>
        </div>
        <div className="card-footer">
          <div className="d-flex justify-content-center align-items-center">
            <a href={`/${url}`} className="text-center text-decoration-none">
              ดูเพิ่มเติม <RightOutlined />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CardPredict;
