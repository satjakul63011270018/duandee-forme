import React from "react";
import "./footer.css";
const Footer = () => {
  return (
    <div className="footer ">
      <p className="text-center text-white pt-3 pb-1 mb-0">
        ติดต่อเราอ่านข้อกำหนดและเงื่อนไขการใช้บริการ
      </p>
      <p className="text-center text-white pt-1"> 2023 © ดวงดี</p>
    </div>
  );
};

export default Footer;
