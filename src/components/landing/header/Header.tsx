import React from "react";
import "./Header.css";
const Header: React.FC = () => {
  return (
    <nav className="navbar navbar-duandee d-flex align-items-center h-100">
      <a className="navbar-brand" href="#">
        <img src="/assets/img/dashboard/logo-navbar.png" alt="navbar-logo" />
      </a>
      <button type="button" className="btn btn-package border-0">
        คุณยังไม่มีแพ็กเกจ
      </button>
      <button type="button" className="btn btn-point border-0 me-2">
        <img src="/assets/img/dashboard/logo-point.svg" alt="navbar-logo" />
        10 คะแนน
      </button>
    </nav>
  );
};

export default Header;
