import React from "react";
import "./menu.css";
const Menubar = () => {
  const menu = [
    {
      id: 1,
      title: "อ่านดวง",
      url: "/",
      icon: "tarot.png",
    },
    {
      id: 2,
      title: "พื้นดวง",
      url: "/",
      icon: "moon.png",
    },
    {
      id: 3,
      title: "โพรไฟล์",
      url: "/",
      icon: "user.png",
    },
    {
      id: 4,
      title: "แจกคะแนน",
      url: "/",
      icon: "star.png",
    },
    {
      id: 5,
      title: "ร้านค้า",
      url: "/",
      icon: "store.png",
    },
  ];
  return (
    <div className="bg-menubar">
      <div className="d-flex align-items-center justify-content-center h-100 w-100">
        {menu.map((item) => (
          <div
            className="card card-menu  border-0 p-0 pt-1 m-0 mx-auto"
            key={item.id}
          >
            <div className="card-body p-0 m-0">
              <div className="d-flex justify-content-center w-100">
                <div className="bg-card-menu">
                  <div className="d-flex justify-content-center align-items-center h-100">
                    <img src={`/assets/img/dashboard/${item.icon}`} alt="" />
                  </div>
                </div>
              </div>
              <p className="p-0 pt-1 m-0 text-center">{item.title}</p>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Menubar;
