import React, { ReactNode, Fragment } from "react";
import Footer from "../footer/Footer";
import Herder from "../header/Header";
import Menubar from "../menubar/Menubar";

interface Props {
  children?: ReactNode;
}

const Layout = ({ children }: Props) => {
  return (
    <Fragment>
      <Herder />
      <Menubar />
      {children}
      <Footer />
    </Fragment>
  );
};

export default Layout;
