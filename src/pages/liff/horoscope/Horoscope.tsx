import React from "react";
import Layout from "../../../components/landing/main/index";
import Tab from "../../../components/dashboard/tab/tab";
import CardDailyAdvice from "../../../components/dashboard/cardDailyAdvice/CardDailyAdvice";
import CardPredict from "../../../components/dashboard/cardPredict/CardPredict";
import CardLuckyNumber from "../../../components/dashboard/cardLuckyNumber/CardLuckyNumber";
import CardPaper from "../../../components/dashboard/cardPaper/CardPaper";

const Horoscope = () => {
  return (
    <Layout>
      <Tab />
      <div className="d-flex justify-content-center mt-2 mb-2 w-100">
        <img src="/public/assets/img/dashboard/banner.png" alt="" />
      </div>
      <CardDailyAdvice />
      <div className="row me-0 ms-0 mb-4">
        <div className="w-50">
          <CardPredict
            img="card-red.png"
            title="สีมงคลของคุณวันนี้"
            url="ad.com"
          />
        </div>
        <div className="w-50">
          <CardPredict img="img-card.png" title="ไพ่ประจำวันนี้" url="ad.com" />
        </div>
      </div>
      <CardLuckyNumber />
      <div className="row me-0 ms-0 ps-2 pe-2 mb-4">
        <div className="w-50">
          <CardPaper
            img="img-card-paper-1.png"
            title="ทำนาย Yes/No"
            url="ทำนาย Yes/No"
          />
        </div>
        <div className="w-50">
          <CardPaper
            img="img-card-paper-2.png"
            title="Wallpaper มงคล"
            url="ทำนาย Yes/No"
          />
        </div>
        <div className="w-50">
          <CardPaper
            img="img-card-paper-3.png"
            title="คาถาบูชา ทริคต่างๆ"
            url="ทำนาย Yes/No"
          />
        </div>
        <div className="w-50">
          <CardPaper
            img="img-card-paper-4.png"
            title="แก้ชง"
            url="ทำนาย Yes/No"
          />
        </div>
      </div>
    </Layout>
  );
};

export default Horoscope;
