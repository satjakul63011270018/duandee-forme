import "./Register.css";

import React from "react";

const Register = () => {
  return (
    <div className="container-fluid bg-img">
      <div className="row">
        <div className="d-flex justify-content-center">
          <img
            src="/public/assets/img/register/logo.png"
            className="img-logo"
            alt=""
          />
        </div>
        <div className="d-flex justify-content-center">
          <div className="card card-main">
            <div className="card-body">
              <h1>กรอกข้อมูลเพื่อผูกดวง</h1>
              <div className="mb-3">
                <label className="form-label">
                  <img
                    src="/public/assets/img/register/icon-date.svg"
                    className="me-1"
                    alt=""
                  />
                  วันเกิด
                </label>
                <div className="d-flex">
                  <select
                    className="form-select me-2"
                    aria-label="Default select example"
                  >
                    <option selected>วันที่</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                  </select>
                  <select
                    className="form-select me-2"
                    aria-label="Default select example"
                  >
                    <option selected>เดือน</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                  </select>
                  <select
                    className="form-select"
                    aria-label="Default select example"
                  >
                    <option selected>ปี</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                  </select>
                </div>
              </div>
              <div className="mb-3">
                <label className="form-label">
                  <img
                    src="/public/assets/img/register/icon-time.svg"
                    className="me-2"
                    alt=""
                  />
                  เวลาเกิด
                </label>
                <div className="row">
                  <div className="col-4">
                    <select
                      className="form-select me-2"
                      aria-label="Default select example"
                    >
                      <option selected>ชม.</option>
                      <option value="1">One</option>
                      <option value="2">Two</option>
                      <option value="3">Three</option>
                    </select>
                  </div>
                  <div className="col-4">
                    <select
                      className="form-select me-2"
                      aria-label="Default select example"
                    >
                      <option selected>นาที</option>
                      <option value="1">One</option>
                      <option value="2">Two</option>
                      <option value="3">Three</option>
                    </select>
                  </div>
                  <div className="col-4">
                    <div className="form-check pt-2">
                      <input
                        className="form-check-input"
                        type="checkbox"
                        value=""
                        id="flexCheckDefault"
                      />
                      <label className="form-check-label">ไม่รู้เวลาเกิด</label>
                    </div>
                  </div>
                </div>
              </div>
              <div className="mb-3">
                <label className="form-label">
                  <img
                    src="/public/assets/img/register/icon-location.svg"
                    alt=""
                    className="me-2"
                  />
                  เลือกจังหวัดที่เกิด
                </label>
                <div className="row">
                  <div className="col-6">
                    <select
                      className="form-select me-2"
                      aria-label="Default select example"
                    >
                      <option selected>เลือกจังหวัด</option>
                      <option value="1">One</option>
                      <option value="2">Two</option>
                      <option value="3">Three</option>
                    </select>
                  </div>
                </div>
              </div>
              <div className="mb-3">
                <label className="form-label">
                  คุณมีโค้ดจากผู้แนะนำหรือไม่? <span>ใช้ฟรี30วัน</span>
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="exampleInputPassword1"
                />
              </div>
              <div className="d-flex justify-content-center">
                <button type="button" className="btn btn-send">
                  ส่งข้อมูล
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;
