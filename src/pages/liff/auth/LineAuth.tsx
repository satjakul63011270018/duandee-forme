// LineAuth.tsx
import { useEffect } from 'react';
import {useLocation, useNavigate } from 'react-router-dom';
// import axios from 'axios';
import liff from "@line/liff";
import './LineAuth.scss'
// Initialize the LIFF SDK
const initLIFF = async () => {
  await liff.init({ liffId: '2000270339-glKP2RNL'});
};
  
const LineAuth = () => {
  const location = useLocation();
  const navigate = useNavigate();

  useEffect(() => {
    const fetchData = async () => {
      await initLIFF();
      await checkAuth();
    }
    fetchData();
  }, []);
  
  const checkAuth = async () => {
    try {
      if (!liff.isLoggedIn()) {
        // Redirect the user to the LINE Login page
        liff.login();
      } else {
        // User is logged in, continue with backend authentication
        const accessToken = liff.getAccessToken();
        console.log(accessToken);
        // await axios.post('/api/verify', { accessToken });
        // Redirect to the intended route after successful login
        const { from } = location.state || { from: { pathname: '/auth/liff/register' } };
        navigate(from, { replace: true }) 
      }
    } catch (error) {
      // Handle error
      console.error('Line Authentication Error:', error);
    }
  };


  return (
    <>
     <div>Loading...</div>
    </>
   
  )
}

export default LineAuth